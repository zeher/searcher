//
//  MediaItem.swift
//  Searcher
//
//  Created by Анастасия Васюра on 14/01/17.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import Foundation

struct MediaItem {
    let author: String
    let icon: URL
    let name: String
}


struct ItunesResponse: Decodable {
    var mediaItems: [MediaItem] = []
    
    init(json: NSDictionary) throws {
        if let items = json["results"] as? [NSDictionary] {
            let parsed = try items.map { try MediaItem(json: $0) }
            self.mediaItems = parsed
        }
    }
}
