//
//  User.swift
//  Searcher
//
//  Created by Анастасия Васюра on 14/01/17.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import Foundation


struct User {
    let link: String
    let avatar: URL
    let username: String
}
