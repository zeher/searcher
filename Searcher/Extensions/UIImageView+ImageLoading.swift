//
//  UIImageView+ImageLoading.swift
//  Searcher
//
//  Created by Vasyura Anastasiya on 23/01/2017.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import UIKit

extension UIImageView {
    
    private enum AssociatedKey {
        static var value: UInt8 = 0
    }
    
    var dataTask: URLSessionDataTask? {
        set {
            objc_setAssociatedObject(self, &AssociatedKey.value, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        get {
            if let task = objc_getAssociatedObject(self, &AssociatedKey.value) as? URLSessionDataTask? {
                return task
            }
            return nil
        }
    }
    
    func loadImage(url: URL) {
        dataTask?.cancel()
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                if let d = data, let image = UIImage(data: d) {
                    self.image = image
                }
            }
        }
        task.resume()
        dataTask = task
    }
}
