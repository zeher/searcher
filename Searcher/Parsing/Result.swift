//
//  Result.swift
//  Searcher
//
//  Created by Анастасия Васюра on 22/01/17.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import Foundation

enum Result<A> {
    case value(A)
    case error(Error)
    
    var value: A? {
        if case .value(let value) = self {
            return value
        } else {
            return nil
        }
    }
}

extension Result {
    
    func map<B>(_ f: (A) -> (B)) -> Result<B> {
        switch self {
        case .value(let value):
            return .value(f(value))
        case .error(let error):
            return .error(error)
        }
    }
    
    func flatMap<B>(_ f: (A) -> Result<B>) -> Result<B> {
        switch self {
        case .value(let value):
            return f(value)
        case .error(let error):
            return .error(error)
        }
    }
}
