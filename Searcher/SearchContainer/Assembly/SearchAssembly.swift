//
//  SearchAssembly.swift
//  Searcher
//
//  Created by Анастасия Васюра on 16/07/2017.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import Foundation

final class SearchAssembly {
    
    static var view: SearchViewController {
        let controller = SearchViewController(with: layout)
        controller.output = presenter
        
        return controller
    }
    
    static var layout: SearchViewLayout {
        return SearchViewLayout()
    }
    
    static var presenter: SearchPresenter {
        let presenter = SearchPresenter()
        presenter.view = view
        
        return presenter
    }
}
