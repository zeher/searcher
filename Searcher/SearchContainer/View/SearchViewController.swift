//
//  SearchViewController.swift
//  Searcher
//
//  Created by Анастасия Васюра on 14/01/17.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import UIKit

final class SearchViewController: UIViewController {
    
    var output: SearchViewOutput!
    var layout: AnySearchViewLayout!
    var searchbarDelegate: SearchBarDelegate!
    var dataDisplayManager: SearchDataDisplayManager!
    
    weak var resultsPageViewController: UIPageViewController!
    weak var segmentedControl: UISegmentedControl!
    weak var searchBar: UISearchBar!
    
    
    init(with layout: AnySearchViewLayout) {
        super.init(nibName: nil, bundle: nil)
        layout.layout(controller: self)
    }
    
    convenience required init(coder aDecoder: NSCoder) {
        self.init(with: SearchViewLayout())
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    func segmentedControlDidChangeValue(_ sender: UISegmentedControl) {
        if let text = searchBar.text, text != "" {
            dataDisplayManager.changePage(with: sender.selectedSegmentIndex, term: text)
        }
    }
}

extension SearchViewController: SearchViewInput {
    
    func setupInitialState() {
        searchBar.delegate = searchbarDelegate
        segmentedControl.addTarget(self, action: #selector(segmentedControlDidChangeValue(_:)), for: .valueChanged)
    }
    
    func configure(with items: [SearchModel]) {
        dataDisplayManager.configure(with: resultsPageViewController, items: items)
    }
}

extension UIViewController {
    
    func addChild(_ child: UIViewController, to parent: UIView? = nil) {
        addChildViewController(child)
        let superview = parent ?? view!
        superview.addSubview(child.view)
        child.didMove(toParentViewController: self)
    }
}


