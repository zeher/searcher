//
//  SearchResultsControllerFactory.swift
//  Searcher
//
//  Created by Анастасия Васюра on 16/07/2017.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import Foundation

protocol AnySearchResultsControllerFactory {
    func searchResultsController(with model: SearchModel) -> SearchResultsViewController
}

class SearchResultsControllerFactory: AnySearchResultsControllerFactory {
    
    func searchResultsController(with model: SearchModel) -> SearchResultsViewController {
        return SearchResultsViewController()
    }
}
