//
//  SearchDataDisplayManager.swift
//  Searcher
//
//  Created by Анастасия Васюра on 16/07/2017.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import UIKit

typealias SearchTerm = String

struct SearchModel {
    
}

protocol SearchDataDisplayManagerDelegate {
    func currentTerm() -> SearchTerm
}

final class SearchDataDisplayManager: NSObject {

    var resultsControllerFactory: SearchResultsControllerFactory!
    
    var delegate: SearchDataDisplayManagerDelegate?
    
    private var pageViewController: UIPageViewController?
    fileprivate var controllers: [SearchResultsViewController] = []
    fileprivate var models: [SearchModel] = []

    func configure(with pageViewController: UIPageViewController, items: [SearchModel]) {
        pageViewController.dataSource = self
        self.models = items
        self.controllers = items.map({ searchModel -> SearchResultsViewController in
            let controller = resultsControllerFactory.searchResultsController(with: searchModel)
            return controller
        })
    }
    
    func changePage(with index: Int, term: SearchTerm) {
        let controller = controllers[index]
        controller.update(with: term)
        pageViewController?.setViewControllers([controller], direction: .forward, animated: false, completion: nil)
    }
    
    func update(with term: SearchTerm) {
        let controller = pageViewController?.viewControllers?.first
        if let searchController = controller as? SearchResultsViewController {
            searchController.update(with: term)
        }
    }
}

extension SearchDataDisplayManager: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let controller = viewController as? SearchResultsViewController  else { return nil }
        let index = controllers.index(of: controller)
        
        if let currentIndex = index, currentIndex < models.count - 1  {
            let nextIndex = currentIndex + 1
            let nextController = controllers[nextIndex]
            
            let term = delegate?.currentTerm() ?? ""
            nextController.update(with: term)
            return nextController
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let controller = viewController as? SearchResultsViewController  else { return nil }
        let index = controllers.index(of: controller)
        
        if let currentIndex = index, currentIndex > 0  {
            let prevIndex = currentIndex - 1
            let prevController = controllers[prevIndex]

            let term = delegate?.currentTerm() ?? ""
            prevController.update(with: term)
            return prevController
        }
        return nil
    }
}
