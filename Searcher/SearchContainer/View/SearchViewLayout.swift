//
//  SearchViewLayout.swift
//  Searcher
//
//  Created by Анастасия Васюра on 16/07/2017.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import UIKit

protocol AnySearchViewLayout {
    func layout(controller: SearchViewController)
}

class SearchViewLayout: AnySearchViewLayout {
    
    func layout(controller: SearchViewController) {
        
        let segmentedControl = UISegmentedControl()
        controller.segmentedControl = segmentedControl
        controller.navigationItem.titleView = segmentedControl
        
        let searchBar = createSearchBar()
        let resultsPageViewController = UIPageViewController()
        
        controller.resultsPageViewController = resultsPageViewController
        controller.addChildViewController(resultsPageViewController)
        resultsPageViewController.didMove(toParentViewController: controller)
        
        let stackView = UIStackView(arrangedSubviews: [searchBar, resultsPageViewController.view])
        stackView.axis = .vertical
        controller.view.addSubview(stackView)
        
        Layout.pinToSuperview(view: stackView)
    }
    
    private func createSearchBar() -> UIView {
        let searchBar = UISearchBar()
        return searchBar
    }
}
