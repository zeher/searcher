//
//  SearchViewInput.swift
//  Searcher
//
//  Created by Анастасия Васюра on 16/07/2017.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import Foundation

protocol SearchViewInput {
    func setupInitialState()
    func configure(with items: [SearchModel]) 
}
