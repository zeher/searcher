//
//  SearchbarDelegate.swift
//  Searcher
//
//  Created by Анастасия Васюра on 16/07/2017.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import UIKit

final class SearchBarDelegate: NSObject, UISearchBarDelegate {
    
    var didChangeText: ((String) -> ())?
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if let text = searchBar.text, text != "" {
            didChangeText?(text)
        }
    }
}
