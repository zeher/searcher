//
//  FullImageViewController.swift
//  Searcher
//
//  Created by Анастасия Васюра on 23/01/17.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import UIKit

class FullImageViewController: UIViewController {
    var image: UIImage?
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        imageView.image = image
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(FullImageViewController.tapImage))
        imageView.addGestureRecognizer(tapRecognizer)
    }
    

    
    func tapImage(_ sender: UIGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
