//
//  SearchResultsModel.swift
//  Searcher
//
//  Created by Анастасия Васюра on 14/01/17.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import UIKit

final class SearchTableModel<Item>: NSObject, TableModel {
    
    var items: [Item] = []
    var handler: () -> () = {_ in}
    var configureCell: (SearchResultsTableViewCell, Item, Bool) -> () = {_ in}
    
    private var reuseID: String { return "SearchResultsTableViewCell" }
    
    func registerCells(_ registerCell: (String, String) -> ()) {
        registerCell("SearchResultsTableViewCell", reuseID)
    }

    var numberOfItems: Int { return items.count }

    func itemAtIndex(_ index: Int) -> Any {
        return items[index]
    }
    
    func reuseID(for item: Any, at index: Int) -> String {
        return reuseID
    }
    
    func configure(cell: SearchResultsTableViewCell, index: Int, item: Any) {
        configureCell(cell, item as! Item, index % 2 != 0)
    }
    
    func update(_ items: [Item]?) {
        self.items = items ?? []
        handler()
    }
    
    func setupHandler(handler: @escaping () -> ()) {
        self.handler = handler
    }
}
