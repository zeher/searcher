//
//  FullImageTransition.swift
//  Searcher
//
//  Created by Vasyura Anastasiya on 24/01/2017.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import UIKit

class FullImageTransition: NSObject, UIViewControllerAnimatedTransitioning {
    
    var isDismiss: Bool
    var imageView: UIImageView?
    
    override init() {
        self.isDismiss = false
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let from = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        let to = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        let containerView = transitionContext.containerView
       
        let source = from?.view as UIView!
        let destination = to?.view as UIView!
        imageView?.isHidden = !isDismiss
        
        guard let imageView = imageView, let sourceView = source, let destinationView = destination else { return }
        
        let snapshotView = isDismiss ? sourceView : destinationView
        if (isDismiss) {
            sourceView.removeFromSuperview()
        }
        let backgroundColor = snapshotView.backgroundColor
        snapshotView.backgroundColor = UIColor.clear
        let sourceRect = containerView.convert(imageView.frame, from: imageView.superview)
        guard let snapshot = snapshotView.snapshotView(afterScreenUpdates: true) else { return }
        
        let beginRect = isDismiss ? snapshot.frame : fit(rect: snapshot.bounds, to: sourceRect)
        let endRect = isDismiss ? fit(rect: snapshot.bounds, to: sourceRect) : snapshot.frame
        let beginColor = isDismiss ? backgroundColor : UIColor.clear
        let endColor = isDismiss ? UIColor.clear : backgroundColor
        
        let coloredView = UIView(frame: destinationView.frame)
        coloredView.backgroundColor = beginColor
        snapshot.frame = beginRect
        containerView.insertSubview(coloredView, aboveSubview: sourceView)
        containerView.insertSubview(snapshot, aboveSubview: coloredView)
        
        let dismiss = isDismiss
        UIView.animate(withDuration: 0.4, animations: {
            coloredView.backgroundColor = endColor
            snapshot.frame = endRect
            }, completion: { complited in
                snapshotView.backgroundColor = endColor
                snapshot.removeFromSuperview()
                coloredView.removeFromSuperview()
                if (!dismiss) {
                    containerView.addSubview(destinationView)
                }
                transitionContext.completeTransition(true)
        })
    }

    
    func fit(rect: CGRect, to sourceRect: CGRect) -> CGRect {
        let koef = min(rect.size.width / sourceRect.size.width, rect.size.height / sourceRect.size.height)
        let size = CGSize(width: rect.size.width / koef, height: rect.size.height / koef)
        let y = sourceRect.origin.y - (size.height - sourceRect.size.height) / 2.0
        let x = sourceRect.origin.x - (size.width - sourceRect.size.width) / 2.0
        return CGRect(origin: CGPoint(x: x, y: y), size: size)
    }

}

