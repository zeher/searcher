////
////  SearchResultsControllerFactory.swift
////  Searcher
////
////  Created by Анастасия Васюра on 21/01/17.
////  Copyright © 2017 Анастасия Васюра. All rights reserved.
////
//
//import UIKit
//
//final class SearchResultsControllerFactory {
//    class func searchResultControllers() -> [SearchResultsViewController] {
//        
//        return [iTunesController(), githubController()]
//    }
//    
//    private class func iTunesController() -> SearchResultsViewController {
//        let iTunesController = UIStoryboard.searchResultsViewController()
//        let iTunesTableModel = SearchTableModel<MediaItem>()
//        iTunesTableModel.configureCell = { cell, item, mirrored in
//            cell.title?.text = item.name
//            cell.detail?.text = item.author
//            cell.mirror(mirrored)
//            cell.iconImageView?.image = nil
//            cell.iconImageView?.loadImage(url: item.icon)
//            
//            cell.didTapImage = { [weak iTunesController] imageView in
//                iTunesController?.showImage(imageView)
//            }
//        }
//
//        iTunesController.model = SearchResultsModelImpl<MediaItem>(tableModel: iTunesTableModel, searchRequest: RequestFactory.iTunesSearchRequest)
//        
//        return iTunesController
//    }
//    
//    private class func githubController() -> SearchResultsViewController {
//        let githubController = UIStoryboard.searchResultsViewController()
//        let githubTableModel = SearchTableModel<User>()
//        githubTableModel.configureCell = { cell, item, mirrored in
//            cell.title?.text = item.username
//            cell.detail?.text = item.link
//            cell.mirror(!mirrored)
//            cell.iconImageView?.image = nil
//            cell.iconImageView?.loadImage(url: item.avatar)
//            
//            cell.didTapImage = { [weak githubController] imageView in
//                githubController?.showImage(imageView)
//            }
//        }
//        githubController.model = SearchResultsModelImpl<User>(tableModel: githubTableModel, searchRequest: RequestFactory.githubSearchRequest)
//        
//        return githubController
//    }
//}
//
//extension UIStoryboard {
//    @nonobjc static let main = UIStoryboard(name: "Main", bundle: nil)
//    
//    static func searchResultsViewController() -> SearchResultsViewController {
//        let viewController = main.instantiateViewController(withIdentifier: "SearchResultsViewController")
//        return viewController as! SearchResultsViewController
//    }
//}
//
//extension SearchResultsTableViewCell {
//    func mirror(_ mirrored: Bool) {
//        let transform = CGAffineTransform(scaleX: mirrored ? -1 : 1, y: 1)
//        contentView.transform = transform
//        iconImageView?.transform = transform
//        title?.transform = transform
//        title?.textAlignment = mirrored ? .right : .left
//        detail?.transform = transform
//        detail?.textAlignment = mirrored ? .right : .left
//    }
//}
