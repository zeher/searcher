//
//  SearchTableView.swift
//  Searcher
//
//  Created by Анастасия Васюра on 14/01/17.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import UIKit

final class TableViewController {

    let tableView: UITableView
    var dataSource: TableViewDataSource?
    
    init(tableView: UITableView) {
        self.tableView = tableView
    }
    
    var tableModel: TableModel? {
        didSet { updateConfiguration() }
    }
    
    private func updateConfiguration() {
        if let tableModel = tableModel {
            dataSource = TableViewDataSource(tableModel: tableModel)
            tableView.dataSource = dataSource

            tableModel.registerCells { nibName, id in
                let nib = UINib(nibName: nibName, bundle: nil)
                tableView.register(nib, forCellReuseIdentifier: id)
            }
            tableModel.setupHandler { _ in
                self.tableView.reloadData()
            }
        } else {
            tableView.dataSource = nil
        }
        tableView.reloadData()
    }
}

final class TableViewDataSource: NSObject, UITableViewDataSource {
    
    let tableModel: TableModel
    
    init(tableModel: TableModel) {
        self.tableModel = tableModel
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableModel.numberOfItems
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let item = tableModel.itemAtIndex(index)
        let reuseID = tableModel.reuseID(for: item, at: index)
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseID, for: indexPath)
        if let cell = cell as? SearchResultsTableViewCell {
            tableModel.configure(cell: cell, index: index, item: item)
        }
        return cell
    }
}

