//
//  TableConfiguration.swift
//  Searcher
//
//  Created by Анастасия Васюра on 14/01/17.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import UIKit

protocol TableModel {
    
    var numberOfItems: Int { get }
    
    func itemAtIndex(_ index: Int) -> Any
            
    func reuseID(for item: Any, at index: Int) -> String
    
    func registerCells(_ registerCell: (String, String) -> ())
    
    func configure(cell: SearchResultsTableViewCell, index: Int, item: Any)
    
    func setupHandler(handler: @escaping ()->()) 
}
