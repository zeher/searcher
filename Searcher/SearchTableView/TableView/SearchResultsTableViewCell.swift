//
//  SearchResultsTableViewCell.swift
//  Searcher
//
//  Created by Анастасия Васюра on 22/01/17.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import UIKit

class SearchResultsTableViewCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    var didTapImage: Sink<UIImageView>?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(SearchResultsTableViewCell.tapImage))
        iconImageView.addGestureRecognizer(tapRecognizer)
    }
    
    override func prepareForReuse() {
        self.iconImageView.image = nil
    }
    
    func tapImage() {
        didTapImage?(iconImageView)
    }
}
