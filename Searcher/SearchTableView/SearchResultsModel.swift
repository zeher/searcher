//
//  SearchResultsModel.swift
//  Searcher
//
//  Created by Анастасия Васюра on 21/01/17.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import Foundation

enum SearchViewState {
    case empty
    case error
    case value([Any])
}

protocol SearchResultsModel {
    var tableModel: TableModel { get }
    func update(with term: String)
    func setupCallback(_ f: @escaping Sink<SearchViewState>)
}

final class SearchResultsModelImpl<Item>: SearchResultsModel {

    var tableModel: TableModel { return searchTableModel }
    
    let searchTableModel: SearchTableModel<Item>
    let createSearchRequest: (String) -> Request<[Item]>
    let sessionManager: SessionManager
    
    var task: URLSessionDataTask?
    var callback: Sink<SearchViewState> = {_ in }
    
    init(tableModel: SearchTableModel<Item>, searchRequest: @escaping (String) -> Request<[Item]>) {
        self.searchTableModel = tableModel
        self.createSearchRequest = searchRequest
        self.sessionManager = SessionManager()
    }
    
    func setupCallback(_ f: @escaping Sink<SearchViewState>) {
        callback = f
    }
    
    func update(with term: String) {
        self.task?.cancel()
        let request = createSearchRequest(term)
//        let task = sessionManager.executeRequest(request: request) { result in
//            self.task?.cancel()
//            let state = self.transformToViewState(result: result)
//            self.callback(state)
//            self.searchTableModel.update(result.value)
//        }
        
     //   self.task = task
    }
    
    func transformToViewState(result: Result<[Item]>) -> SearchViewState {
        switch result {
        case .value(let value):
            return value.count != 0 ? .value(value) : .empty
        default:
            return .error
        }
    }
}
