//
//  SearchResultsViewController.swift
//  Searcher
//
//  Created by Анастасия Васюра on 14/01/17.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import UIKit

final class SearchResultsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyStateView: UIView!
    @IBOutlet weak var errorStateView: UIView!
    
    let fullImageTransition: FullImageTransition = FullImageTransition()
    
    var model: SearchResultsModel? {
        didSet {
            setupTableModel()
        }
    }
    
    var tableViewController: TableViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewController = TableViewController(tableView: tableView)
        setupTableModel()
    }
    
    func search(query: String) {
        model?.update(with: query)
    }
    
    func setupTableModel() {
        if let model = model {
            tableViewController?.tableModel = model.tableModel
            model.setupCallback { [weak self] state in
                self?.updateState(state)
            }
        }
    }
    
    func updateState(_ state: SearchViewState) {
        self.emptyStateView.isHidden = true
        self.errorStateView.isHidden = true
        self.tableView.isHidden = true
        
        switch state {
            case .empty:
                self.emptyStateView.isHidden = false
            case .error:
                self.errorStateView.isHidden = false
            case .value(_):
                self.tableView.isHidden = false
        }
    }
    
    func showImage(_ imageView: UIImageView) {
        fullImageTransition.imageView = imageView
        let storyboard = UIStoryboard.mainStoryboard()
        let fullImageController = storyboard.instantiateViewController(withIdentifier: "FullImageViewController")
        fullImageController.modalPresentationStyle = .custom
        if let controller = fullImageController as? FullImageViewController {
            controller.image = imageView.image
            controller.transitioningDelegate = self

            self.present(controller, animated: true)
        }
    }
}

extension SearchResultsViewController {
    func setup(with model: SearchModel) {
        
    }
    
    func update(with term: SearchTerm) {
        
    }
}

extension UIStoryboard {
    class func mainStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        return storyboard
    }
}

extension SearchResultsViewController: UIViewControllerTransitioningDelegate {

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        fullImageTransition.isDismiss = false
        return fullImageTransition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        fullImageTransition.isDismiss = true
        return fullImageTransition
    }
}

