//
//  RequestFactory.swift
//  Searcher
//
//  Created by Анастасия Васюра on 22/01/17.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import Foundation

enum RequestFactory {

    static func githubSearchRequest(query: String) -> Request<[User]> {
        let escapedQuery = query.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let urlString = "https://api.github.com/search/users?q=\(escapedQuery!)"
        let url = URL(string: urlString)
        let request = Request<[User]>(
            url: url!,
            decode: Parsing.decodeGitHubResponse(json:)
        )
        return request
    }

    static func iTunesSearchRequest(query: String) -> Request<[MediaItem]> {
        let escapedQuery = query.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let urlString = "https://itunes.apple.com/search?term=\(escapedQuery!)"
        let url = URL(string: urlString)
        let request = Request<[MediaItem]>(
            url: url!,
            decode: Parsing.decodeITunesResponse(json:)
        )
        return request
    }

}
