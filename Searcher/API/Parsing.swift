//
//  Parsing.swift
//  Searcher
//
//  Created by Анастасия Васюра on 14/01/17.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import Foundation

struct Request<A> {
    let url: URL
    let decode: (NSDictionary) throws -> A
}

typealias Sink<A> = (A) -> ()

protocol Decodable {
    init(json: NSDictionary) throws
}

extension Request where A: Decodable {
    
    init(url: URL) {
        self.url = url
        decode = A.init(json:)
    }
}

extension String: Error {}

enum Parsing {
    
    private static func getValue(from json: NSDictionary, path: String) throws -> Any {
        if let part = json[path] {
            return part
        }
        throw "missing field \(path)"
    }
    
    static func get<A: Decodable>(json: NSDictionary, path: String) throws -> A {
        let value = try getValue(from: json, path: path)
        if let json = value as? NSDictionary {
            return try A(json: json)
        }
        throw "type mismatch"
    }
    
    static func getArray<A: Decodable>(json: NSDictionary, path: String) throws -> [A] {
        let value = try getValue(from: json, path: path)
        if let json = value as? [NSDictionary] {
            return try json.map { try A(json: $0) }
        }
        throw "type mismatch"
    }
    
    static func getString(json: NSDictionary, path: String) throws -> String {
        let value = try getValue(from: json, path: path)
        if let string = value as? String {
            return string
        }
        throw "type mismatch"
    }
    
    static func getURL(json: NSDictionary, path: String) throws -> URL {
        let value = try getValue(from: json, path: path)
        if let string = value as? String {
            if let url = URL(string: string) {
                return url
            }
        }
        throw "type mismatch"
    }
}

extension Parsing {
    static func decodeGitHubResponse(json: NSDictionary) throws -> [User] {
        return try getArray(json: json, path: "items")
    }
    
    static func decodeITunesResponse(json: NSDictionary) throws -> [MediaItem] {
        return try getArray(json: json, path: "results")
    }
}

extension MediaItem: Decodable {
    
    init(json: NSDictionary) throws {
        self.author = try Parsing.getString(json: json, path: "artistName")
        self.icon = try Parsing.getURL(json: json, path: "artworkUrl100")
        self.name = try Parsing.getString(json: json, path: "trackName")
    }
}

extension User: Decodable {
    init(json: NSDictionary) throws {
        self.link = try Parsing.getString(json: json, path: "url")
        self.avatar = try Parsing.getURL(json: json, path: "avatar_url")
        self.username = try Parsing.getString(json: json, path: "login")
    }
}
