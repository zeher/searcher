//
//  SearchService.swift
//  Searcher
//
//  Created by Анастасия Васюра on 14/01/17.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import Foundation

final class SessionManager {
    
    let session: URLSession = URLSession.shared
    
    func f(url: URL, handler: @escaping Sink<Result<Data>>) -> URLSessionDataTask {
        let task = session.dataTask(with: url) { data, response, error in
            if let error = error {
                handler(.error(error))
            } else if let data = data {
                handler(.value(data))
            } else {
                handler(.value(Data()))
            }
        }
        task.resume()
        return task
    }
    
}

protocol AnySearchService {
    func search<A>(request: Request<A>, handler: @escaping Sink<Result<A>>) -> URLSessionDataTask
}

final class SearchService: AnySearchService {
    
    let sessionManager: SessionManager
    
    init(with sessionManager: SessionManager) {
        self.sessionManager = sessionManager
    }
    
    func search<A>(request: Request<A>, handler: @escaping Sink<Result<A>>) -> URLSessionDataTask {
        
        return sessionManager.f(url: request.url) { result in
            
            let parsed: Result<A> = result.flatMap { data in
                do {
                    let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
                    
                    if let container = jsonObject as? NSDictionary {
                        let parsed = try request.decode(container)
                        return .value(parsed)
                    } else {
                        return .error("type mismatch")
                    }
                } catch {
                    return .error(error)
                }
            }
            DispatchQueue.main.async {
                handler(parsed)
            }
        }
    }
}
