//
//  LayoutHelper.swift
//  Searcher
//
//  Created by Анастасия Васюра on 16/07/2017.
//  Copyright © 2017 Анастасия Васюра. All rights reserved.
//

import UIKit

enum Layout {
    
    static func pinToSuperview(view: UIView) {
        guard let superview = view.superview else { return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: superview.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: superview.trailingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
    }
}
